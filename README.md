# About

This is a tiny program designed to be copied onto devices based on MIPS Broadcom
router SoCs. It disables the feature Broadcom calls Flow Cache. Flow Cache is
a performance optimisation on the broadcom chips which bypasses some of the
kernel machinery when processing packets on existing TCP connections. However,
some versions of it are broken when handling packets involving TCP inside tunnels
(see https://ttlexpired.co.uk/2016/02/12/ipv6-tunnel-and-failing-tcp-sessions/
for a more in depth), and so turning this feature off can prevent the router from
dropping all packets after the initial SYN and SYN/ACK when tunneling.

In order to use this, you will need shell access to your router. For some TP-Link
devices (e.g. my device, which is a TD-W9977), you can follow the instructions at
https://github.com/sta-c0000/tpconf_bin_xml in the README to get root access and
execute commands each time the router boots up.

If your device already has a program from Broadcom called `fcctl`, then you should
be able to just use `fcctl disable`. However, if your device doesn't come with that
utility, the program here sends the same ioctl to the kernel module to disable
the Flow Cache - that is the only capability it has.

Note: Flow Cache is only disabled until you reboot the device, so you need to
run this program every time the device boots.

I also have included a pre-built binary in prebuilt/killfc that you can use if
you prefer not to built it yourself.

# Design goals

The program is designed to be static - so it can run on a range of
different MIPS32 Broadcom-SoC based devices. It is also designed to have a
very small compiled size - the stripped ELF binary comes in at 1140 bytes -
so it can be easy to get on to devices even when there are limited means to
transfer it, and won't use much space on the device.

The program has also been designed to be easy to cross-compile on Linux
without the need for building other parts of the device's firmware.

At present, it is MIPS specific. Some ARM based Broadcom SoCs might also
have a similar requirement, but more work would be required to make it
compile for those architectures.

# Building it

On Debian, install `gcc-8-mips-linux-gnu` and `libc6-dev-mips-cross` and
`binutils-mips-linux-gnu`.

Check out this repo and run `make`. The binary will be built at the
top level of the repo.

# Getting it on the device

Once you have root access, you could install a tftp server on another system,
copy killfc into it, and use `cd /tmp; tftp -l killfc -g 10.1.2.5 69` (assuming
a system with busybox tftp on it). As it is relatively small, you could also
encode it as hex escapes and echo it to a file. You will need to use
`chmod +x killfc` to make it executable.

# Running it

The program doesn't take any arguments and has no UI - when you run it, it
immediately attempts to disable flow cache.

To keep it small, the program doesn't print error messages. The exit codes
tell you what happened:
* Exit code 1: Couldn't open the Flow Cache device. You might be running it
  on a device that it isn't appropriate for, or not as root.
* Exit code 2: Couldn't execute the ioctl. The device might be the wrong type
  or something else might be blocking it from being disabled.
