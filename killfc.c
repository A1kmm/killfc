/*
Copyright 2020 Andrew Miller

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <linux/unistd.h>

#define FCACHE_DISABLE_IOCTL 0x67

#define SYS3(nsys, arg1, arg2, arg3) ({\
  register void* a0 asm("$4") __attribute__ ((unused)) = (void*)arg1;   \
  register void* a1 asm("$5") __attribute__ ((unused)) = (void*)arg2;   \
  register void* a2 asm("$6") __attribute__ ((unused)) = (void*)arg3;   \
  register void* a3 asm("$7") __attribute__ ((unused));   \
  register int v0 asm("$2") __attribute__ ((unused)) = nsys;\
  asm("syscall"\
      : "=r"(v0), "=r"(a3) \
      : "r"(a0), "r"(a1), "r" (a2) \
      : "$1", "$3", "$8", "$9", "$10", "$11", "$12", "$13", \
        "$14", "$15", "$24", "$25", "hi", "lo", "memory");\
   a3 == 0 ? v0 : -v0;                                           \
  })

void __start() {
  int exitcode;

  int fd = SYS3(SYS_open, "/dev/fcache", O_RDWR, 0);
  if (fd >= 0) {
    if (SYS3(SYS_ioctl, fd, FCACHE_DISABLE_IOCTL, 0) != 0)
      exitcode = 2;
    else
      exitcode = 0;
    SYS3(SYS_close, fd, 0, 0);
  } else
    exitcode = 1;
  SYS3(SYS_exit, exitcode, 0, 0);
}
